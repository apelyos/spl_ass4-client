#include "WhatsappHandler.h"
#include "Utils.h"
#include "boost/thread.hpp"

const int HTTP_OK = 200;
const int MSG_POOL_TIME = 2000;

WhatsappHandler::WhatsappHandler(ConnectionHandler *connection) : _connection(connection) , _token("") {
	_threadShouldRun = new bool(false);
}

WhatsappHandler::~WhatsappHandler() {
	delete _threadShouldRun;
}

void WhatsappHandler::dequeueMessagesThread() {
	while (*_threadShouldRun == true) {
		string result = queue();
		if (result.find("No new messages") == string::npos) {
			cout << result;
		}
		
		boost::this_thread::sleep(boost::posix_time::milliseconds(MSG_POOL_TIME));
	}
}

string WhatsappHandler::handleReply(HttpResponse &httpResponse) {
	if (httpResponse.statusCode == HTTP_OK) {
		// if we have cookie, set it
		map<string, string>::iterator it = httpResponse.headers.find("Set-Cookie");
		if (it != httpResponse.headers.end()) {
			vector<string> auth = split(it->second, '='); // form of 'user_auth=blabla'
			if (auth.size() > 1 && !auth[1].empty()) { // valid cookie
				_token = auth[1]; // set cookie
				logger("saving cookie: " + _token);
			}
		}

		return httpResponse.messageBody + "\n";
	}
	else {
		return "Status Code: " + toString(httpResponse.statusCode) + ", Message: " + httpResponse.messageBody + "\n";
	}
}

string WhatsappHandler::handleReply(string &rawReply) {
	HttpResponse httpResponse; 
	
	// try to parse http response
	try {
		httpResponse = parseHttpResponse(rawReply);
	}
	catch (exception &e) {
		return e.what();
	}
	
	return (handleReply(httpResponse));
}

string WhatsappHandler::login(string &userName, string &phone){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	bodyParams["UserName"] = userName;
	bodyParams["Phone"] = phone;

	_connection->sendToken(formulateHttpRequest("POST", "/login.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);

	HttpResponse httpResponse;
	try {
		httpResponse = parseHttpResponse(reply);
	}
	catch (exception &e) {
		return e.what();
	}

	//if we got valid response and the thread is not already running and we got security token
	if (httpResponse.statusCode == HTTP_OK && *_threadShouldRun == false && 
		httpResponse.messageBody.find("ERROR") == string::npos) {
		*_threadShouldRun = true;
		boost::thread workerThread(&WhatsappHandler::dequeueMessagesThread, this);
	}
	
	return handleReply(httpResponse);
}

string WhatsappHandler::logout(){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;

	_connection->sendToken(formulateHttpRequest("GET", "/logout.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	 

	HttpResponse httpResponse;
	try {
		httpResponse = parseHttpResponse(reply);
	}
	catch (exception &e) {
		return e.what();
	}

	if (httpResponse.statusCode == HTTP_OK) {
		_token = ""; // Invalidates the cookie
		*_threadShouldRun = false;
	}

	return handleReply(httpResponse);
}

string WhatsappHandler::queue(){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;

	_connection->sendToken(formulateHttpRequest("GET", "/queue.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::listUsers(){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["List"] = "Users";

	_connection->sendToken(formulateHttpRequest("POST", "/list.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::listGroups(){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["List"] = "Groups";

	_connection->sendToken(formulateHttpRequest("POST", "/list.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::listGroup(string &groupName){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["List"] = "Group";
	bodyParams["Group"] = groupName;

	_connection->sendToken(formulateHttpRequest("POST", "/list.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::sendGroup(string &groupName, string &message){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["Type"] = "Group";
	bodyParams["Target"] = groupName;
	bodyParams["Content"] = message;

	_connection->sendToken(formulateHttpRequest("POST", "/send.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::sendUser(string &phoneNum, string &message){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["Type"] = "Direct";
	bodyParams["Target"] = phoneNum;
	bodyParams["Content"] = message;

	_connection->sendToken(formulateHttpRequest("POST", "/send.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::add(string &groupName, string &phoneNum){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["Target"] = groupName;
	bodyParams["User"] = phoneNum;

	_connection->sendToken(formulateHttpRequest("POST", "/add_user.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::createGroup(string &groupName, string &phonesList){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["GroupName"] = groupName;
	bodyParams["Users"] = phonesList;

	_connection->sendToken(formulateHttpRequest("POST", "/create_group.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}

string WhatsappHandler::remove(string &groupName, string &phoneNum){
	map<string, string> bodyParams;
	map<string, string> headerParams;
	headerParams["Cookie"] = "user_auth=" + _token;
	bodyParams["Target"] = groupName;
	bodyParams["User"] = phoneNum;

	_connection->sendToken(formulateHttpRequest("POST", "/remove_user.jsp", headerParams, bodyParams));
	string reply;
	_connection->getToken(reply);
	return handleReply(reply);
}
