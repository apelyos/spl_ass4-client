#ifndef UTILS_H_
#define UTILS_H_

#include <string>

void logger(string msg);

void cleanChar(string &mystring, char ch);

string joinStrings(vector<string> &strings, char delimiter, int startIndex, int endIndex);

vector<string> split(string &sentence, char delimiter);

vector<string> splitByLines(string &sentence);

string url_encode(const string &value);

string url_decode(string &SRC);

string &trim(std::string &s);

string toString(int num);

int stoi(string &str);

#endif /* UTILS_H_ */