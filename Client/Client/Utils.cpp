#define _CRT_SECURE_NO_WARNINGS
#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

using namespace std;

const bool ENABLE_LOGGER = false;

// Convenient logger function
void logger(string msg) {
	if (ENABLE_LOGGER)	cout << "DEBUG: " << msg << endl;
}

// trim string from start
std::string &ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim string from end
std::string &rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim string from both ends
std::string &trim(std::string &s) {
	return ltrim(rtrim(s));
}

void cleanChar(string &mystring, char ch) {
	mystring.erase(std::remove(mystring.begin(), mystring.end(), ch), mystring.end());
}

string joinStrings(vector<string> &strings, char delimiter, int startIndex, int endIndex) {
	string result = "";

	for (int i = startIndex; i < endIndex; i++) {
		result += strings[i];

		if (i != endIndex - 1) 
			result += delimiter;
	}

	return result;
}

// Split a sentence into words by a specified delimiter character
vector<string> split(string &sentence, char delimiter){
	string curr = "";

	vector<string> res;

	for (unsigned i = 0; i < sentence.length(); i++) {
		if (sentence[i] != delimiter) {
			curr += sentence[i];
		}
		else {
			if (!curr.empty()){
				res.insert(res.end(), curr);
			}
			curr = "";
		}
	}

	if (!curr.empty()){
		res.insert(res.end(), curr);
	}

	return res;
}

vector<string> splitByLines(string &sentence){
	string curr = "";

	vector<string> res;

	cleanChar(sentence,'\r');

	for (unsigned i = 0; i < sentence.length(); i++) {
		if (sentence[i] != '\n') {
			curr += sentence[i];
		}
		else {
			res.insert(res.end(), curr);
			curr = "";
		}
	}

	if (!curr.empty()){
		res.insert(res.end(), curr);
	}

	return res;
}

string url_encode(const string &value) {
	ostringstream escaped;
	escaped.fill('0');
	escaped << hex;

	for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
		string::value_type c = (*i);

		// Keep alphanumeric and other accepted characters intact
		if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
			escaped << c;
			continue;
		}

		// Any other characters are percent-encoded
		escaped << '%' << setw(2) << int((unsigned char)c);
	}

	return escaped.str();
}

string url_decode(string &SRC) {
	string ret;
	char ch;
	unsigned long int i;
	int ii;
	for (i = 0; i<SRC.length(); i++) {
		if (int(SRC[i]) == 37) {
			sscanf(SRC.substr(i + 1, 2).c_str(), "%x", &ii);
			ch = static_cast<char>(ii);
			ret += ch;
			i = i + 2;
		}
		else {
			ret += SRC[i];
		}
	}
	return (ret);
}

// convert int to string
string toString(int num) {
	ostringstream convert;   // stream used for the conversion
	convert << num;      // insert the textual representation of 'Number' in the characters in the stream
	return convert.str();
}

// convert string to int
int stoi(string &str) {
	return atoi(str.c_str());
}