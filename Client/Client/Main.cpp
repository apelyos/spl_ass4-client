#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include "Main.h"
#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "boost/algorithm/string.hpp"
#include "Poco/StreamCopier.h"
#include "connectionHandler.h"
#include "WhatsappHandler.h"
#include "Utils.h"

using namespace std;

const string CMD_ERR = "Please enter a valid command.\n";

// This is a useful template function to convert a string into any
// type T for which a stream reader operation is defined.
template <typename T>
bool from_string(T& t,
	const std::string& s,
	std::ios_base& (*f)(std::ios_base&) = std::dec)
{
	std::istringstream iss(s);
	return !(iss >> f >> t).fail();
}

void usage(char **argv)
{
	std::cerr << "Usage: " << argv[0] << " <host> <port>" << std::endl;
}

vector<string> getUserCommand() {
	std::string line;
	cout << ">";
	getline(cin, line);
	line = trim(line);

	while (line.empty()) {
		cout << CMD_ERR;
		cout << ">";
		getline(cin, line);
		line = trim(line);
	}

	return split(line, ' ');
}

void listCommands() {
	cout << "*** Available Commands ***\n"
		"Login [Username] [Phone]\n"
		"Logout\n"
		"List Users\n"
		"List Groups\n"
		"List Group [Group Name]\n"
		"Send Group [Group Name] [Message]\n"
		"Send User [Phone] [Message]\n"
		"CreateGroup [Group Name] [User#1,User#2,...,User#n]\n"
		"Add [Group Name] [Phone]\n"	
		"Remove [Group Name] [Phone]\n"
		"Exit\n\n"; 
}

int main(int argc, char **argv)
{
	cout << "Welcome to this awesome WhatsApp client.\nAll rights reserved to Yos & Igal.\n\n";

	if (argc != 3){
		usage(argv);
		return 1;
	}

	std::string host(argv[1]);
	unsigned short port;
	if (!from_string(port, argv[2])){
		usage(argv);
		return 1;
	}

	try {
		ConnectionHandler *connectionHandler = new ConnectionHandler(host, port);

		if (!connectionHandler->connect()) {
			std::cerr << "Cannot connect to " << host << ":" << port << std::endl;
			return 1;
		}

		WhatsappHandler *whatsappHandler = new  WhatsappHandler(connectionHandler);
		

		listCommands();

		vector<string> userCommand = getUserCommand();

		while (true) {

			// Commands...
			if (boost::iequals(userCommand[0], "login") && userCommand.size() == 3) {
				cout << whatsappHandler->login(userCommand[1], userCommand[2]);
			}
			else if (boost::iequals(userCommand[0], "logout") && userCommand.size() == 1) {
				cout << whatsappHandler->logout();
			}
			else if (boost::iequals(userCommand[0], "list") && userCommand.size() >= 2) {
				if (boost::iequals(userCommand[1], "users") && userCommand.size() == 2) {
					cout << whatsappHandler->listUsers();
				}
				else if (boost::iequals(userCommand[1], "groups") && userCommand.size() == 2) {
					cout << whatsappHandler->listGroups();
				}
				else if (boost::iequals(userCommand[1], "group") && userCommand.size() == 3) {
					cout << whatsappHandler->listGroup(userCommand[2]);
				}
				else {
					cout << CMD_ERR;
				}
			}
			else if (boost::iequals(userCommand[0], "send") && userCommand.size() >= 4) {
				if (boost::iequals(userCommand[1], "user")) {
					cout << whatsappHandler->sendUser(userCommand[2], joinStrings(userCommand, ' ', 3, userCommand.size()));
				}
				else if (boost::iequals(userCommand[1], "group")) {
					cout << whatsappHandler->sendGroup(userCommand[2], joinStrings(userCommand, ' ', 3, userCommand.size()));
				}
				else {
					cout << CMD_ERR;
				}
			}
			else if (boost::iequals(userCommand[0], "add") && userCommand.size() == 3) {
				cout << whatsappHandler->add(userCommand[1], userCommand[2]);
			}
			else if (boost::iequals(userCommand[0], "remove") && userCommand.size() == 3) {
				cout << whatsappHandler->remove(userCommand[1], userCommand[2]);
			}
			else if (boost::iequals(userCommand[0], "queue") && userCommand.size() == 1) { // HIDDEN COMMAND.. SHHHHHH....
				cout << whatsappHandler->queue();
			}
			else if (boost::iequals(userCommand[0], "createGroup") && userCommand.size() == 3) { // HIDDEN COMMAND.. SHHHHHH....
				cout << whatsappHandler->createGroup(userCommand[1], userCommand[2]);
			}
			else if (boost::iequals(userCommand[0], "exit") && userCommand.size() == 1) {
				whatsappHandler->logout();
				connectionHandler->close();
				delete whatsappHandler;
				delete connectionHandler;
				return 0;
			}
			else if (boost::iequals(userCommand[0], "?")) { // List commands
				listCommands();
			}
			else {
				cout << CMD_ERR;
			}

			userCommand = getUserCommand();
		}
	}
	catch (exception &e) {
		cout << "Error: " << e.what();
	}
}