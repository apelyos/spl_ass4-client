#ifndef WHATSAPPHANDLER_H_
#define WHATSAPPHANDLER_H_

#include "connectionHandler.h"
#include "HttpHandler.h"
#include <string>
#include <cstdlib>
#include <stdio.h>


using namespace std;

class WhatsappHandler {
private:
	ConnectionHandler *_connection;
	string _token;
	string handleReply(HttpResponse &httpResponse);
	string handleReply(string &rawReply);
	void dequeueMessagesThread();
	bool *_threadShouldRun;

public:
	WhatsappHandler(ConnectionHandler *connection);
	virtual ~WhatsappHandler();

	string login(string &userName, string &phone);
	string logout();
	string queue();
	string listUsers();
	string listGroups();
	string listGroup(string &groupName);
	string sendGroup(string &groupName, string &message);
	string sendUser(string &phoneNum, string &message);
	string add(string &groupName, string &phoneNum);
	string createGroup(string &groupName, string &phonesList);
	string remove(string &groupName, string &phoneNum);
};


#endif /* WHATSAPPHANDLER_H_ */