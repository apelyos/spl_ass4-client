#include "HttpHandler.h"
#include "Utils.h"


const string DELIM = "\r\n";
const string SPACE = " ";
const string HTTPV = "HTTP/1.1";
const int HTTP_OK = 200;

string formulateHttpRequest(string verb, string resource, map<string, string> &headerParams, map<string, string> &bodyParams) {
	string result = "";
	// main
	result.append(verb + SPACE + resource + SPACE + HTTPV + DELIM);

	//headers
	for (map<string, string>::iterator iterator = headerParams.begin(); iterator != headerParams.end(); iterator++) {
		result.append(iterator->first + ": " + iterator->second + DELIM);
	}
	result.append(DELIM);

	//body
	//URL Encoded
	for (map<string, string>::iterator iterator = bodyParams.begin(); iterator != bodyParams.end(); iterator++) {
		result.append(iterator->first + "=" + url_encode(iterator->second));

		if (iterator != --bodyParams.end()) {
			result.append("&");
		}
	}

	result.append(DELIM);

	logger("HttpRequset:\n" + result);

	return result;
}

HttpResponse parseHttpResponse(string &response) {
	HttpResponse httpResponse;

	logger("HttpResponse:\n" + response);

	vector<string> lines = splitByLines(response);
	bool reachedBody = false;
	bool processedTop = false;

	for (vector<string>::iterator it = lines.begin(); it != lines.end(); ++it) {
		vector<string> words = split(*it, ' ');

		// top header
		if (!processedTop && words.size() > 1 && words[0].compare(HTTPV) == 0) {
			if (!words[1].empty()) {
				httpResponse.statusCode = stoi(words[1]);
				processedTop = true;
			}
			else {
				throw new exception("Malformed top header");
			}
		}
		else if (processedTop && (words.size() == 0 || words[0].empty())) { // check if were at body
			reachedBody = true;
		}
		else if (processedTop && words.size() > 1 && !reachedBody) { // at headers
			if (!words[1].empty()) {
				cleanChar(words[0],':');
				httpResponse.headers[words[0]] = words[1];
			}
			else {
				throw new exception("Malformed headers");
			}
		} 
		else if (processedTop && reachedBody) {  // at body
			httpResponse.messageBody += *it;

			if (it != --lines.end()) {
				httpResponse.messageBody += DELIM;
			}
		} 
	}

	return httpResponse;
}