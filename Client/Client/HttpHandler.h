#ifndef HTTP_HANDLER__
#define HTTP_HANDLER__

#include "HttpHandler.h"
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <vector>
#include <map>
#include <iostream>

using namespace std;

struct HttpResponse {
	int statusCode;
	std::map<std::string, std::string> headers;
	std::string messageBody;
};

std::string formulateHttpRequest(string verb, string resource, map<string, string> &headerParams, map<string, string> &bodyParams);

HttpResponse parseHttpResponse(string &response);

#endif //HTTP_HANDLER__